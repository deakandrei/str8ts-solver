## A [str8ts](https://www.str8ts.com/) solver.

The program uses the basic strategies presented on the
[site](https://www.str8ts.com/Str8ts_Strategies.html),
without naked pairs/triples strategy in combination with
a simple depth-first search that guarantees we reach a
solution if there is one.